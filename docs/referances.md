# Referances page

I will list web referances used to build this project here once i have compiled a list of them.

## Web interactions

[beautiful soup](https://beautiful-soup-4.readthedocs.io/en/latest/)
[selenium](https://www.selenium.dev/documentation/webdriver/getting_started/install_library/)
[web scrapeing toutorial 1](https://realpython.com/beautiful-soup-web-scraper-python/)
[web scrapeing toutorial 2](https://medium.com/analytics-vidhya/web-scraping-using-selenium-79a2fcc77215)

## GUI

[pysimplegui](https://docs.pysimplegui.com/en/latest/)

## exe conversion

[auto-py-to-exe tool](https://pypi.org/project/auto-py-to-exe/)
[tool website](https://nitratine.net/blog/post/issues-when-using-auto-py-to-exe/?utm_source=auto_py_to_exe&utm_medium=readme_link&utm_campaign=auto_py_to_exe_help#what-is-auto-py-to-exe)
