# Future Features

The plans for the next version of this program are currently

I will not be impelmenting any of these on this application but in its new version Impact_assistant

## sort feature

### sort_settings functions

the goals of sort functions are the following use cases

- as a user i want to see only what matches my open schedule and i can bid on nothing else
- as a user i want to see every position that meets my schedule
- as a user i want to see every position that matches a list of titles
- as a user i want to see only positions that matches a list of titles and i can bid on

## settings

### Theme

as a user i would like to be able to change the color theme

## Timer

i would like to add a timer that times the return speed from click of "get work" or "bid for work" to results.

## Reminder feature

- users will then be reminded that priority is not set you will have to arange them on your list but they will be in date order for the more part
- provide users with link and save options as well as exit the program

## save log feature

- as a user i would like to see and a beable to save a log of what has been bid on for record keeping.
