# Change Log

## version

### Development branch version

1.2.0

### Production Branch Version

1.2.0

## Version Notes

### 0.0 INIT

- 0.1 File initilazation and tree built out
- 0.2: Main program files brought over from source
- 0.3: Got content scraper working and set up for repo onlline
- 0.5: Pushed to online repo in the stages mentioned prevously

### 0.1 First Major Update to idea code

- 0.1.1: security Patch
- 0.1.2: code cleanup and modularization. objects and constructors fixed
- 0.1.3: final code cleanup ready for version update

### 0.2 User_interface setup

- 0.2.1: starting work on Desktop UI doing file prep and creating space for work
- 0.2.2: built out file structure for user interface and menu
- 0.2.3: user window loop working with good calls to web scraping
- 0.2.4: job data cleaned and formated to be user friendly
- 0.2.5: basic data returning to User interface, needs cleaning but it is working.

#### ***UPDATE TO HOW I AM DOCUMENTING see README under Contributions***

### 0.3: user_interface_results : 07-17-24

- 0.3.1: added branch level to user_interface will now be createing user_interface_"peice of ui"
- 0.3.2: cleaned up the visual of the tree when displaying jobs
- 0.3.3: added icon instead of checkbox for ease of useing tree
- 0.3.4: added red check (hope to make it x) for un-avail positions
- 0.3.5: added empty box for avail positions
- 0.3.6: cleaning up docs structure created instructions.md for adding to docs
- 0.3.7: got most of the bid jobs working pushing to get assistance.
- 0.3.8: Added data-id to positions for tracking back and clicking on returns. Also got Indexing of Checkboxes working properly.
- 0.3.8: got check and x boxes working properly so you cant click on an x and check boxes function
- 0.3.9: completed data cleanup for jobs cominng in and displaying in tree still need to decide a solution to position dates!
- 0.3.9.1: closeing out this set of branches and pushing to dev to start work on other features.

### 0.4: Sort_and_Bid

- 0.4.0: Docs update to new version and work being done see Future feature and worknotes in docs for more details. setting up to work on Sorting data and cleaning up bidding on work... there will end up being whole code overhaul in this branch.
- 0.4.1: Bid on work complete and functioning. the program will now return all available work for the current month(a bug i will be adding to the list). and you can bid on green check items with a checklist and a bid on work button. I feel that i am Close to my MVT about 4 days early.
- 0.4.2: Settings window structured with tabs for auth and search settings to be built in to them.
- 0.4.3: Settings windows needed to be individualized for save functionality of diferent cfg files. now working with file save and recall with auto creation on missing file. MASS FILE CLEANUP AND REARANGE FOR MY BRAIN things moved around abit.
- 0.4.4: Settings changed back to single window due to complications in reading multipul cfg files... will adress later. sort by green only is working and implemented in settings, and i have almost fixed the seeing past the end of the month bug.
- 0.4.5: got the final bugs worked out of loading second month, and check marks fixed... green only returns are still not sorting quite right but are closer to functioning properly. Next step is prioritising looks and useablity.
- 0.4.5: connected user settings and removed env from the system. updated docs. getting close to a thery of alpha testing to 5 or so individuals with UI access and code knowlage.
- 0.4.6: green only sort is now working and bids now accomidated soreted click data proprely. the functions for this branch are met.
- 0.4.7: bug fixes to bid_jobs to get working again. with sort. program main functions are opperational last work before mvp is cleaning up displays.

### 0.5: User Interface Improvements

- 0.5.0: docs and code cleanup and first branch creation, getting ready to work on User Interface for the program. Full Wording refactor will happen in this update group as well.
- 0.5.1: Fixing Data returns and displays
- - due to limitations of tabel and tree having to change data flow a bit with a ui restructure to inclued a details pane and real checkboxes thus eliminateing the need for clickable checks in tree. will replace them with color icons for job bidablity.
- - Added new frame for data showing clicked tree item still has bugs but is displaying.
- 0.5.2: Fixed new data frame to update not repeat. Have tested with a few events.
- 0.5.3: Removing old check and converting it to Icon.
- - cleaned up date presentation in data frame
- - cleaned up results frame presentation
- 0.5.4: Refactor of bid on work to  accomidate new changes from 0.5.3 swaped check for a btn. added disableing feature to bid and bid on work. tested and confirmed that bidding works.
- 0.5.5: Added a settings fucntion to turn on or off standby buttons globaly. (This is for dev for now but a time save feature for later as well.)

### 0.6: Final MVP Cleanup pre Alpha Testing

- 0.6.0: Document update and prep for exe conversion testing. this will clean up files on the git.
- 0.6.1: exe test push(FAILURE)
- 0.6.2: Docs cleanup and additions. Instructions for download and install added. req.txt updated with full list of reqs

### 1.0: MVP complete

- 1.0.0: Due to distirbution and use issues the platform for this app needs to change as such this will be the last update to this application. It is functional as is and will do a basic of what it is intended to accomplish.
- 1.1.1 : added safty check for id and pass before
- 1.1.2: cleaned up documents, fixed a bug with bid jobs. added minimzation line to driver.connect adding in screenshots to readme.
- 1.1.3: more docs and tweeks branch not respected

### 1.2.0 Cleaned up for test distribution

- 1.2.1 small bug fixes and such
