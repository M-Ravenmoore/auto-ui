import time
import PySimpleGUI as sg
from os import path

"""File imports"""
from pages.main_window import main_window
from pages.main_frames.results_frame import results_frame
from pages.main_frames.data_frame import data_frame
from pages.about import about_popup
from pages.options.settings import settings_popup,DEFAULT_SETTINGS, ele_keys

from helper_func.menu_fx.options_fx.save_settings import save_settings
from helper_func.menu_fx.options_fx.load_settings import load_settings
from helper_func.impact_fx.login import impact_login
from helper_func.impact_fx.bid_jobs import bid_jobs
from helper_func.impact_fx.job_fetch.get_jobs import get_jobs
from helper_func.driver_connect import driver_connect
from helper_func.impact_fx.sort_clicks import sort_clicks

SETTINGS_FILE = path.join(path.dirname(__file__), './config_files/config.cfg')

url_impact = 'https://iatse15.unionimpact.com'

def main():
    jobs_list = []
    display_list = []
    bid_id_list = []
    bid_list =[]
    data = {}
    window, settings = None, load_settings(SETTINGS_FILE,DEFAULT_SETTINGS,ele_keys)
    while True:
        
        if window is None:
            window = main_window(settings)
        
        event,values = window.read()
        print("event and values at 26",event,values)
        
        if event == sg.WIN_CLOSED or event == "Exit":
            break
        
        if event == "-BID BTN-":
            driver = driver_connect(True)
            time.sleep(1)
            impact_login(driver,url_impact,settings)
            print('BIDS LIST',bid_id_list,bid_list)
            
            sorted_data = sort_clicks(bid_list)
            bids = {}            
            for job, pos in sorted_data:
                reduced_job = int(job[1:])-1
                reduced_pos = int(pos[1:])-1
                url = display_list[reduced_job].url
                postition_id = display_list[reduced_job].positions[reduced_pos].data_id
                bids.setdefault(reduced_job, {"url": url, "positions": []})["positions"].append(postition_id)
            
            bid_jobs(driver,bids)
     
        if event == '-GET WORK BTN-':
            if '@' not in settings['user_email'] or settings['password'] == "":
                sg.popup('Please set your Impact credentials')
                
            else:
                driver = driver_connect(True)
                impact_login(driver,url_impact,settings)
                jobs_list = []
                display_list = []
                jobs_list = get_jobs(driver,url_impact)
                driver.close()
                
                if settings['green_checks_only'] : display_list = jobs_list.green_only() 
                else: display_list = jobs_list.get_all_jobs() 
                
                if settings['standby_enable']:window['-STANDBY BTN-'].update(disabled=False)
                else:window['-STANDBY BTN-'].update(disabled=True)
                    
                results = results_frame(display_list)
                window['r_frame'].update(visible=True)  
                window.extend_layout(window['r_frame'],results)
                

        if event == "About...":
            about_popup() 
        
        if event =="Settings":
            event,values = settings_popup(settings).read(close=True)            
            if event == "-SAVE SETTINGS-":
                window.close()
                window = None
                save_settings(SETTINGS_FILE,ele_keys,settings, values)
        
        if event == '-BID-':
            window['-BID BTN-'].update(disabled=False)
            window['-BID-'].update(disabled=True)
            bid_id_list.append(data['pos_data_id'])
            bid_pos = values['--JOBS_TREE--'][0]
            if bid_pos not in bid_list:
                bid_list.append(bid_pos)
             
        elif event == '--JOBS_TREE--':
            print('event and values',event,values)
            
            pos_clicked = values['--JOBS_TREE--'][0]
            
            if 'P' in pos_clicked: 
                data = data_frame(pos_clicked,display_list)
                print(data)
                window['d_frame'].update(visible = True)
                window['-E TITLE-'].update(data['e_title'])
                window['-E VENUE-'].update(data['e_venue'])
                window['-E ADDRESS-'].update(data['e_address'])
                window['-EMPLOYER-'].update(data['employer'])
                window['-E DATES-'].update(data['e_dates'])
                window['-POS TITLE-'].update(data['pos_title'])
                window['-POS DATES-'].update(data['pos_dates'])
                window['-POS NOTES-'].update(data['pos_notes'])

                if data['bidable'] == True and data['pos_data_id'] not in bid_id_list: 
                    window['-BID-'].update(disabled=False)
                    
                else:
                    window['-BID-'].update(disabled=True)
    window.close()
     
def auto_ui():
    main()
    
auto_ui()