# auto-UI

## Name

Auto-UI.

## Version

### Development branch version

1.0.1
[gitpages dev Branch](https://gitlab.com/M-Ravenmoore/auto-ui/-/tree/dev?ref_type=heads)

### Production Branch Version

1.0.0
[gitpages Main Branch](https://gitlab.com/M-Ravenmoore/auto-ui/-/tree/main?ref_type=heads)
[download](https://tinyurl.com/impact-assist)

## Description

A data scraper and handler for my union job board

## Installation

### Normal Users

Click [here](https://app.box.com/s/urm6gspieigki4clxan2cijwkc0q3npu) and download the application zip file.
unzip the file some where you can find it on your computer.
open file and double click the Program to run.

On first run the GUI will tell you you need to sign up or use the trial.
signing up is free.
The next version of this software will use a diferent system entirly.

#### WARNING

many antivirus/defender programs have been known to Flag this due to the way it is packaged i am looking in to a new exe handler but untill then you may have to add an allow rule to your antivirus for the exe file to un zip properly.
IT IS NOT A VIRUS here here is a link to the known bug.

[virus bug](https://nitratine.net/blog/post/issues-when-using-auto-py-to-exe/#my-antivirus-detected-the-exe-as-a-virus)

### Programers

- bring down repo from [project](https://gitlab.com/M-Ravenmoore/auto-ui)
`git clone`
- setup a virtual enviroment
- boot that enviroment with
- (if needed)install Python3 and pip you can google that if you need to
- install requirements by useing
`pip install -r .\requriements.txt`
- run program
`python3 .\auto_ui.py`
- set your login credents in settings
- - if working on dev set test mode to on to shorten response time from skimmer
- click the button and watch it work.

## Visuals

![Start of Program](docs/img/just_opened.png)
![events](docs/img/events_frame.png)
![after clicking](docs/img/All_frames.png)
![data blowup](docs/img/Data_frame_close.png)
![events blowup](docs/img/Result_frame_close.png)

### pysimplegui

users will be prompted to create a free acount with pysimlegui in order to use the product.

## Support

Reach out on [Discord](https://discord.gg/FERuMPXtp4)
or email the developer at <m.ravenmoore@gmail.com>
or submit a issue on [gitlab](https://gitlab.com/M-Ravenmoore/auto-ui/-/issues)

## Roadmap

This is a testing prototype as of 6/15/2024 and will nolonger be added to substantialy but will be maintained for the next 5 to 10 weeks while data is collected.

## Contributing to Code

This Repo is closed to updates!

## Authors and acknowledgment

Matthew Ravenmoore (Developer)
Chris Hudson (Mentor helping with code and packageing)

Further acknowledgments can be found [here](docs/referances.md)

## License

MIT Licence

## Project status

This project is NO LONGER IN active development.

Due to distribution issues and other useage needs the project will be rebuilt and further development of this project can be found
[here](https://gitlab.com/M-Ravenmoore/impact_assistant)

## Last Updated

Last updated on 06-20-2024 by Matt Ravenmoore
