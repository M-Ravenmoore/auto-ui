import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)

def save_settings(settings_file,keys, settings, values):
    print(keys,'keys in save')
    if values:      # if there are stuff specified by another window, fill in those values
        for key in keys:  # update window with the values read from settings file
            try:
                settings[key] = values[keys[key]]
            except Exception as e:
                print(f'Problem updating settings from window values. Key = {key}')

    with open(settings_file, 'w') as f:
        jsondump(settings, f)

    sg.popup('Settings saved')