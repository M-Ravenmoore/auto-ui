import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)

from helper_func.menu_fx.options_fx.save_settings import save_settings

def load_settings(settings_file, default_settings, keys):
    print(settings_file,'keys in load')
    try:
        with open(settings_file, 'r') as f:
            settings = jsonload(f)
    except Exception as e:
        sg.popup_quick_message(f'exception {e}', 'No settings file found... will create one for you', keep_on_top=True, background_color='red', text_color='white')
        settings = default_settings
        # Will need to become a save all settings function as we add auth and other settings files
        save_settings(settings_file, keys, settings, None)
    return settings