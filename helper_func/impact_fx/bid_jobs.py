from selenium.webdriver.common.by import By
import time
from helper_func.impact_fx.job_fetch.get_positions import get_positions

def bid_jobs(driver,bids): 
    count = 0
    bid_record = []
    print('this is bids',bids.items())
    for idx,item in bids.items():
        # print('this is bids and pos:',idx,item)
        pos = item['positions']    
        driver.get(item['url'])        
        positions_list =  get_positions(driver)       
        for position in positions_list:
            if any(p == position.data_id for p in pos):
                button = driver.find_element(By.ID, 'select_hob')
                button.click()
                time.sleep(2)
                driver.find_element(By.CLASS_NAME,'add-job-to-my-job-pick').click()
                time.sleep(5)
                count +=1
                bid_record.append(position)
                
    print('jobs bid for successfuly, total bids',count)
                    
    driver.close()
    return bid_record, count