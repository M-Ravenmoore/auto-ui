class Job_item():
    
    def __init__(self, title: str = ''):
        self.title = title
        self.id = ''
        self.address = ''
        self.url = ''
        self.contact = ''
        self.venue = ''
        self.employer = ''
        self.notes = ''
        self.dates = []
        self.close_date = ''
        self.positions = []
        self.contact_email = ''
        self.contact_phone = ''
        self.status = ''
        self.worker_count = ''
        self.percent_complete = ''
        self.days_to_close = ''
        self.contract_url = ''
    
    def __dict__(self):
        return { 
                "title" : self.title,
                "id":self.id,
                "address":self.address,
                "url":self.url,
                "contact":self.contact,
                "venue":self.venue,
                "employer":self.employer,
                "notes":self.notes,
                "dates":self.dates,
                "close date":self.close_date,
                "positions":self.positions,
                "contact email":self.contact_email,
                "contact phone":self.contact_phone,
                "status":self.status,
                "worker count":self.worker_count,
                "percent cpmelete":self.percent_complete,
                "days to close":self.days_to_close,
                "contract url":self.contract_url,
                }