class Position_item():
    __title = ''
    __notes = ''
    __req_num = ''
    __dates = []
    __icon_color = ''
    __data_id = ''
    __icon = int

    def __init__(self,title:str=None):
        if title is not None:
            self.__title = title
    
    @property
    def title(self):
        return self.__title
    @title.setter
    def title(self, value:str):
        self.__title = value
    
    @property
    def notes(self):
        return self.__notes
    @notes.setter
    def notes(self, value:str):
        self.__notes = value
    
    @property
    def icon(self):
        return self.__icon
    @icon.setter
    def icon(self,value:int):
        self.__icon = value

    @property
    def req_num(self):
        return self.__req_num
    @req_num.setter
    def req_num(self, value:str):
        self.__req_num = value
        
    @property
    def data_id(self):
        return self.__data_id
    @data_id.setter
    def data_id(self, value:str):
        self.__data_id = value
        
    @property
    def dates(self):
        return self.__dates
    @dates.setter
    def dates(self, value):
        self.__dates = value

    @property
    def icon_color(self):
        return self.__icon_color
    @icon_color.setter
    def icon_color(self,value:str):
        self.__icon_color = value