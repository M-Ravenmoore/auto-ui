from helper_func.impact_fx.constructors.job_constructor import Job_item

class Jobs():
    __jobs = []
    def __init__(self):
        print('looking for work')
        self._current = -1
    
    def __iter__(self):
        return self
    
    def __next__(self):
        if self._current < len(self.__jobs)-1:
            self._current +=1
            return self.__jobs[self._current]
        else:
            self._current = -1
        raise StopIteration

    def __len__(self):
        return len(self.__jobs)
   
    def add_job(self,job:Job_item):
        self.__jobs.append(job)
    
    def get_all_jobs(self):
        return self.__jobs
        
    def green_only(self):
        green_only_jobs = []
        for job in self.__jobs:
            temp = []
            for x in job.positions:
                 if x.icon_color == "rgba(63, 182, 24, 1)":temp.append(x)
            job.positions = temp
            if job.positions:green_only_jobs.append(job)
        return green_only_jobs
    
    @property
    def jobs(self)->list:
        self.__jobs
    
    @classmethod
    def show_jobs(cls):
        print('*'*80)
        print('Jobs')
        print('*'*80)

        count = 1
        if len(cls.__jobs) == 0:
            print("no jobs found")
        else:
            for job in cls.__jobs:
                print(count, job.title, job.url, job.venue, job.dates)
                count +=1
            print('end of list')