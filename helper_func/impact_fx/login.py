from selenium.webdriver.common.by import By

# NOTE This needs try catch where if useremail and pass arnt set it fails with a pop up error telling you to set credents.

def impact_login(driver, url_impact,settings):
    __user_email = settings['user_email']
    __password = settings['password']
    driver.get(url_impact + "/login")
    input_email = driver.find_element(By.ID, 'email')
    input_pass = driver.find_element(By.ID, 'password')
    input_email.send_keys(__user_email)
    input_pass.send_keys(__password)
    driver.find_element(By.ID, 'submit-btn').click()
    return driver