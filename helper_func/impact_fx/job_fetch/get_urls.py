from bs4 import BeautifulSoup as bs

job_urls = []

def get_urls (driver):
    html = driver.page_source
    soup = bs(html, features = 'html.parser')
    row_ele = soup.find_all('td', class_= 'fc-event-container')
    for event in row_ele:        
        job_urls.append(event.find('a',href=True)['href'])
    print('urls added',job_urls)
    return(job_urls)