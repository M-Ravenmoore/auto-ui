from selenium.webdriver.common.by import By

from bs4 import BeautifulSoup as bs
from helper_func.impact_fx.constructors.position_constructor import Position_item

def get_positions(driver):
    positions = driver.find_element(By.CLASS_NAME, 'jobs')
    all_positions = positions.find_elements(By.CLASS_NAME, 'template-download')
    data_list = []
    html = driver.page_source
    soup = bs(html, features = 'html.parser')
    data_ele = soup.find_all('tr',{"class":"template-download",'data-id':True}['data-id'])
    for hit in data_ele:
        data_list.append(str(hit['data-id']))

    pos_count = len(all_positions)
    pos_list = []
    
    # NOTE switch from i to itteritave when i have time to look deeper to remove redundant code
    i =0
    for row in all_positions:
        info_items = row.find_elements(By.TAG_NAME, 'td')
        data = []
        ico_color = ''
        position = Position_item()
        
        if row.find_element(By.CLASS_NAME, "btn-circle").is_displayed() == True:
            ico_color = row.find_element(By.CLASS_NAME, "btn-circle").value_of_css_property('background-color')
        
        if ico_color == "rgba(63, 182, 24, 1)": position.icon = 1 #green (able to bid)
        if ico_color == "rgba(255, 255, 0, 1)": position.icon = 2 #yellow (filled but within skill)
        if ico_color == "rgba(39, 128, 227, 1)" :position.icon = 3 #blue (out of skill)
        if ico_color == "rgba(255, 0, 57, 1)": position.icon = 4 # red (already bid)

        for item in info_items: 
            data.append(item.text)
            
        position.title = data[0]
        position.notes = data[1]
        position.dates = data[3]
        position.req_num = data[2]
        position.icon_color = ico_color
        position.data_id = data_list[i]
        i += 1
        pos_list.append(position)
    print(pos_count,"positions added")
    return pos_list