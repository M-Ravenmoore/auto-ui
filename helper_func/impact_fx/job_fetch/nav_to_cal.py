import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

def nav_to_cal(driver, driver_url):
    driver.get(driver_url + '/dp/events')
    driver.find_element(By.ID, 'event-calendar').click()
    select_element = driver.find_element(By.NAME, 'status')
    select = Select(select_element)
    # wait for dropdown
    time.sleep(2)
    driver.find_element(By.CSS_SELECTOR, 'option[value="2"]')
    select.select_by_value('2')
    # wait for grid to refresh to calendar
    time.sleep(2)