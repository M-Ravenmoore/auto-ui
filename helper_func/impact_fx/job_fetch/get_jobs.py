import time
from datetime import date
from selenium.webdriver.common.by import By

from helper_func.impact_fx.job_fetch.get_urls import get_urls
from helper_func.impact_fx.job_fetch.get_positions import get_positions
from helper_func.impact_fx.constructors.job_constructor import Job_item
from helper_func.impact_fx.job_fetch.data_clean import data_cleaner
from helper_func.impact_fx.job_fetch.nav_to_cal import nav_to_cal
from helper_func.impact_fx.classes.jobs_class import Jobs 

def get_jobs(driver, driver_url): 
    jobs = Jobs()
    all_urls = []
    job_urls = []    
    # scrape jobs from calendar 
    nav_to_cal(driver,driver_url)
    all_urls += get_urls(driver)
    today = date.today()
    # if halfway thorugh the month look at next month too
    if int(today.day) > 15:
        print('need next month too')
        nav_to_cal(driver,driver_url)
        next_btn = driver.find_element(By.CSS_SELECTOR,'button[aria-label="next"]')
        next_btn.click()
        time.sleep(1)
        all_urls += get_urls(driver)  
    for url in all_urls:
            if url not in job_urls:
                job_urls.append(url)
       
    job_count = 1
    
    # NOTE implement setting to turn this on or off
    # test_urls = [job_urls[0],job_urls[1],job_urls[2],job_urls[3],job_urls[4]]
    # for url in test_urls:
    for url in job_urls:
        job = Job_item()
        driver.get(url)
        job.url = url

        # get job information
        box_ele = driver.find_element(By.CLASS_NAME, 'row-table')
        # time.sleep(1.7)
        job_data = box_ele.find_elements(By.CLASS_NAME, 'row')
        
        data_table = data_cleaner(job_data)
        positions =  get_positions(driver)            
        job.title = data_table[2]
        job.contact = data_table[3]
        job.employer = data_table[1]
        job.venue = data_table[0]
        job.address = data_table[6]
        job.dates = data_table[7]
        job.notes = data_table[8]
        job.close_date = data_table[12]
        job.positions = positions

        jobs.add_job(job)
        print('job added', job_count,job)
        job_count += 1
    return jobs