def data_cleaner(data):
    temp_data = []
    final_data= []
    for data_item in data:
        # print("this is data coming in",data)        
        temp_data.append(data_item.text.splitlines())
        # print('this is tempdata after first filter', temp_data)
    for item in temp_data:
        for sub_item in item:
            final_data.append(sub_item)
    return final_data