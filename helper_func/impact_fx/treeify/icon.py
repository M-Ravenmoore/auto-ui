from PIL import Image, ImageDraw
from io import BytesIO

def icon(check):
    box = (32, 32)
    background = (255, 255, 255, 0)
    rectangle = (3, 3, 29, 29)
    line = ((9, 17), (15, 23), (23, 9))
    cross_line_l = ((9,23),(23,9))
    cross_line_r = ((9,9),(23,23))
    im = Image.new('RGBA', box, background)
    draw = ImageDraw.Draw(im, 'RGBA')
    draw.rectangle(rectangle, outline='black', width=3)
    
    if check == 1:
        draw.line(line, fill='green', width=3, joint='curve')
    if check == 2:
        draw.line(cross_line_l, fill='yellow', width=3,)
        draw.line(cross_line_r, fill='yellow', width=3,)
    if check == 3:
        draw.line(cross_line_l, fill='blue', width=3,)
        draw.line(cross_line_r, fill='blue', width=3,)    
    elif check == 4:
        draw.line(line, fill='red', width=3, joint='curve')
       
    with BytesIO() as output:
        im.save(output, format="PNG")
        png = output.getvalue()
    return png

