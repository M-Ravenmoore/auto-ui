import PySimpleGUI as sg

from helper_func.impact_fx.treeify.icon import icon
check = [icon(0), icon(1), icon(2), icon(3), icon(4)]

def treeify_jobs(jobs_list):
    jobs_treedata = sg.TreeData()
    n = 1
    for open_job in jobs_list:
        job_key = "J" + str(n)
        jobs_treedata.Insert("", job_key, n, [open_job.title,open_job.venue,""])
        k = 1
        for pos in open_job.positions:
            pos_key = job_key + ".P"+ str(k)
            if pos.icon == 1:
                jobs_treedata.Insert(job_key, pos_key, k, [pos.title,pos.notes,pos.req_num], icon = check[pos.icon])
                k +=1 
            else:
                jobs_treedata.Insert(job_key, pos_key + ":x", k, [pos.title,pos.notes,pos.req_num], icon= check[pos.icon])
                k +=1
        n += 1
    return jobs_treedata