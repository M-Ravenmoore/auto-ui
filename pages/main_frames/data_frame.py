import PySimpleGUI as sg

def data_frame(pos,disp_list):
    click = pos.split('.')
    bidable = True
    print('this is clean_clicks in dataframe',click)
    j = click[0]
    if ':x' in click[1]: 
        bidable = False
        p_click = click[1].split(':')
        p = p_click[0]
    else:
        p = click[1]
    
    j_idx = int(j[1:])-1
    p_idx = int(p[1:])-1
        
    event_url = disp_list[j_idx].url
    event_title = disp_list[j_idx].title
    event_venue = disp_list[j_idx].venue
    venue_address = disp_list[j_idx].address
    employer = disp_list[j_idx].employer
    date_range = disp_list[j_idx].dates
    # contact_phone = disp_list[j].phone
    # contact_email = disp_list[j].email
    # contract = disp_list[j].contract_url
    
    pos_title = disp_list[j_idx].positions[p_idx].title
    pos_dates = disp_list[j_idx].positions[p_idx].dates
    pos_notes = disp_list[j_idx].positions[p_idx].notes
    pos_data_id = disp_list[j_idx].positions[p_idx].data_id
    
    data = {
        'e_url': event_url,
        'e_title': event_title,
        'e_venue': event_venue,
        'e_address': venue_address,
        'employer': employer,
        'e_dates': date_range,
        'pos_title': pos_title,
        'pos_dates': pos_dates,
        "pos_notes": pos_notes,
        'pos_data_id': pos_data_id, 
        'bidable': bidable    
    }
    return(data)