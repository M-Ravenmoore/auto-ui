import PySimpleGUI as sg
from helper_func.impact_fx.treeify.treeify_jobs import treeify_jobs

def results_frame(disp_list):
    tree_data = treeify_jobs(disp_list)
    headings = ["Title","notes",'# req']
    
    results = [[sg.pin(sg.Tree(
                    data = tree_data,
                    headings = headings,
                    auto_size_columns = True,
                    row_height = 40,
                    enable_events = True,
                    key = "--JOBS_TREE--",
                    expand_x = True,
                    expand_y = True,
                    col0_width = 10,
                    num_rows=10,
                    select_mode=sg.TABLE_SELECT_MODE_BROWSE,
                    metadata = []
                    ))]]
    return results
    