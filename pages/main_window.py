import PySimpleGUI as sg

def main_window(settings):
    
    sg.set_options(suppress_raise_key_errors=True,
                   suppress_error_popups=False, 
                   suppress_key_guessing=False)
    
    menu_def = [['&File',['---','E&xit']],
                ['&Options',['Settings']],
                ['&Help',['&About...']]]
# move to settings
    sg.theme('Dark Blue 3')
    
    frame_layout1 = [[sg.Text('Welcome to Auto-UI')],
                     [sg.Text('Please set Credentials before running your first search')],
                     [sg.Text('Detailed search settings are located in Options')],
                     [sg.Button('Look For Work',key='-GET WORK BTN-',enable_events=True,),sg.Button('Bid on work',key='-BID BTN-',enable_events=True,disabled=True)]]
    
    event_data_layout=  [[sg.Text('Event: ', key = '-E TITLE-')],
                         [sg.Text('contact_name', key = '-CONTACT NAME-')],
                         [sg.Text('Venue: ', key = '-E VENUE-')],
                         [sg.Text('Employer:', key = '-EMPLOYER-')],
                         [sg.Text('Address: ', key = '-E ADDRESS-')],
                         [sg.Text('Contact Phone/Email: ', key = '-CONTACT INFO-')],
                         [sg.Text('Dates: ', key = '-E DATES-')],
                         [sg.Text('Contract:', key = '-CONTRACT URL-')],
                         [sg.Text('Notes', key = '-E NOTES-')]]
    
    position_data_layout = [[sg.Text('Position: ', key = '-POS TITLE-')], 
                            [sg.Text("Dates: ", key = '-POS DATES-')],
                            [sg.Text('Notes: ',key = '-POS NOTES-')],
                            [sg.Button('Bid?', key = '-BID-',enable_events=True),sg.Button('Stand-By?', key = '-STANDBY BTN-',enable_events=True)]]
    
    position_detail_layout  = [[sg.Frame("event Details", event_data_layout, key='e_details')],
                               [sg.Frame('Position detiails',position_data_layout, key='p_details')]]
    
    results_layout= [[]]
    
    layout = [[sg.Menu(menu_def)],
              [sg.Frame('Info Frame', frame_layout1)],
              [sg.pin(sg.Frame('Results Frame', results_layout, key = 'r_frame',visible = False)),sg.pin(sg.Frame('Data Frame', position_detail_layout,key = 'd_frame', visible = False))]]

    window = sg.Window('Auto-U.I. Tool V_1.2',layout, size = (1000,800),resizable=True)

    
    return window