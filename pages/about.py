import PySimpleGUI as sg

# from PIL import (Image, ImageTk)

def about_popup():
    # remake for new gui
    sg.theme('Dark Blue 3')  # please make your windows colorful

    welcome_txt = "Welcome to the Prototype of Impact_Assist (Auto_U.I.) this program is the first working version of an attempt to make Union impact Data readable and sortable."
    about_frame_layout = [[sg.Text(welcome_txt)]]

    layout = [[sg.Text("About The Program")],
              [sg.Frame('About',about_frame_layout)],
              [sg.Button("Exit")]]

    pop_window = sg.Window('About', layout)
    

    while True:
        event,values = pop_window.read()
        if event == sg.WIN_CLOSED or event == "Exit":
            pop_window.close()
            break


    # imageHead = Image.open("img/HEAD.png")
    # resize_image = imageHead.resize((250,250))
    # img = ImageTk.PhotoImage(resize_image)