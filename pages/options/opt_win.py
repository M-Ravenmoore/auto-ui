import PySimpleGUI as sg

def opt_win(layout):
    
    sg.set_options(suppress_raise_key_errors=True,
                   suppress_error_popups=False, 
                   suppress_key_guessing=False)

    sg.theme('Dark Blue 3')

    pop_window = sg.Window('Options',layout, size = (600,600),resizable=True,keep_on_top=True, finalize=True)
    return pop_window