import PySimpleGUI as sg
from json import (load as jsonload, dump as jsondump)

from pages.options.opt_win import opt_win

job_types = ['Payroll Steward','Payroll Steward - Assistant','Head Rigger','Rigger-Ground','Rigger-Open Beam (up)- Large Venue','Rigger-Open Beam (up)','Truck Loader','Fork Certified','Grip - General','FOH Spot']

DEFAULT_SETTINGS = {'green_checks_only':False,'user_email':'','password':'','standby_enable':False}

ele_keys = {'green_checks_only':'-GREEN ONLY-','user_email':'-USER EMAIL-','password':'-PASSWORD-','standby_enable':'-STANDBY I/O-'}

def settings_popup(settings):
    def TextLabel(text): return sg.Text(text+':', justification='r', size=(15,1))
    
    # turn this back in to tabs??
    
    settings_layout = [[sg.Text('Log In Settings')],
                            [TextLabel('User Name'),sg.Input(key='-USER EMAIL-')],
                            # should obscure the input for password
                            [TextLabel('Password'),sg.Input(key='-PASSWORD-')],
                            [sg.Text('Search Settings')],
                            [TextLabel('Green Check Only'),sg.Checkbox('filter by green checks',key='-GREEN ONLY-')],
                            
                            # Add more settings here
                            [TextLabel('Standby enabled?'),sg.Checkbox('standby enable',key='-STANDBY I/O-')],
                            
                            [sg.Text("Remember To save your settings befere moving from this window!!"), sg.Button("Save" ,key="-SAVE SETTINGS-")]]
    
    pop_window = opt_win(settings_layout)
    
    for key in ele_keys:   # update window with the values read from settings file
       try:
           pop_window[ele_keys[key]].update(value=settings[key])
       except Exception as e:
            print(f'Problem updating PySimpleGUI window from settings. Key = {key}')

    return pop_window